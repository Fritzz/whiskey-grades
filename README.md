# README

## The challenge

```
1: Create a website where I can grade the whiskeys that I have had in the past.

2: Per whiskey, I would like to define the title, a small description and give it grades for taste, color, and smokiness.

3: The data should be stored in a RDBS.

4: I want to be able to search for whiskeys based on the title, description and/or minimum grade.

Below requirements must be fulfilled:

● The backend should be in Ruby on Rails.
● A minimum of two models should be used with a has many relations.
● Searching should be done in the backend
● The frontend should be enriched with React, so react component to make the page dynamic (not a full SPA)
● Tests should be written in RSpec.

```

Done:

- backend in Ruby on Rails
- (imported data from https://whiskyanalysis.com/index.php/database/)
- Minimum of 2 models
- backend filtering

Not done:

- tests
- implemented react


## To run this locally

- install ruby 3.0 using rvm or rbenv
- run `bundle install`
- run `rails db:setup`
- run `rails s`
