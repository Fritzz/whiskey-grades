class GradesFilter
  include ActiveModel::Model

  attr_accessor :name, :description, :taste_score, :color_score, :smokiness_score

  def apply
    scope = Grade.includes(:whiskey)
    scope = scope.with_min_taste_score(taste_score) if taste_score.present?
    scope = scope.with_min_color_score(color_score) if color_score.present?
    scope = scope.with_min_smokiness_score(smokiness_score) if smokiness_score.present?
    scope = scope.with_whiskey_name(name) if name.present?
    scope = scope.with_whiskey_description(description) if description.present?
    scope
  end

end
