class Country < ApplicationRecord
  has_many :whiskeys

  validates :name, presence: true, uniqueness: true

  default_scope { order(:name) }
end
