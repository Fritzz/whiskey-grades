class WhiskeysFilter
  include ActiveModel::Model

  attr_accessor :name, :description

  def apply(scope)
    scope = scope.with_name(name) if name.present?
    scope = scope.with_description(description) if description.present?
    scope
  end

end
