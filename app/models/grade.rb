class Grade < ApplicationRecord
  SCALE = 1..10

  belongs_to :whiskey
  attr_accessor :name, :description

  validates :taste, :color, :smokiness, numericality: {
    greater_than_or_equal_to: SCALE.first, less_than_or_equal_to: SCALE.last, only_integer: true
  }

  scope :with_min_taste_score, ->(score) { where('taste >= ?', score) }
  scope :with_min_color_score, ->(score) { where('color >= ?', score) }
  scope :with_min_smokiness_score, ->(score) { where('smokiness >= ?', score) }
  scope :with_whiskey_name, ->(term) { joins(:whiskey).merge(Whiskey.with_name(term)) }
  scope :with_whiskey_description, ->(term) { joins(:whiskey).merge(Whiskey.with_description(term)) }

end
