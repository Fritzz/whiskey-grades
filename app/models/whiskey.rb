class Whiskey < ApplicationRecord
  COST_OPTIONS = {
    '$' => 1,
    '$$' => 2,
    '$$$' => 3,
    '$$$$' => 4,
    '$$$$$' => 5,
    '$$$$$+' => 6
  }


  has_many :grades
  belongs_to :category
  belongs_to :country_of_origin, class_name: 'Country'

  validates :name, uniqueness: true
  enum cost: COST_OPTIONS

  default_scope { order(:name) }
  scope :with_name, ->(term) { where("whiskeys.name ILIKE :term", term: "%#{term}%") }
  scope :with_description, ->(term) { where("whiskeys.description ILIKE :term", term: "%#{term}%") }

end
