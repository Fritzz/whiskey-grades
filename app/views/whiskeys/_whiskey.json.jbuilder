json.extract! whiskey, :id, :created_at, :updated_at
json.url whiskey_url(whiskey, format: :json)
