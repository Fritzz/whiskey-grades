class CreateWhiskeys < ActiveRecord::Migration[6.1]
  def change
    create_table :countries do |t|
      t.string :name
    end

    create_table :categories do |t|
      t.string :name
    end

    create_table :whiskeys do |t|
      t.string :name
      t.string :description
      t.decimal :metacritic_score, precision: 4, scale: 2
      t.integer :metacritic_score_count
      t.integer :cost
      t.references :country_of_origin, foreign_key: { to_table: 'countries' }
      t.references :category
      t.timestamps
    end

    create_table :grades do |t|
      t.references :whiskey
      t.integer :taste
      t.integer :color
      t.integer :smokiness
      t.timestamps
    end
  end
end
