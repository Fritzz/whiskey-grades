# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).

# This is not the most efficient import of data, but fast enough, and runs just once, so that's not a problem.
puts "Of course, the app is more fun when we already have some data available."
puts "Lets get a nice starting point with some data from https://whiskyanalysis.com/index.php/database/"

whiskey_data = CSV.read('db/seeds/whiskeys/WhiskyDatabase.csv', headers: true)
puts "Importing #{whiskey_data.size} whiskeys..."
whiskey_data.map do |whiskey|
  Whiskey.create({
    name: whiskey['Whisky'],
    metacritic_score: whiskey['Meta Critic'],
    metacritic_score_count: whiskey['#'],
    cost: whiskey['Cost'],
    country_of_origin: Country.find_or_create_by(name: whiskey['Country']),
    category: Category.find_or_create_by(name: whiskey['Type'])
  })
end
puts "Done. Imported #{Whiskey.count} whiskeys."
