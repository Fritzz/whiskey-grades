Rails.application.routes.draw do

  root to: 'whiskeys#index'

  resources :countries
  resources :grades
  resources :whiskeys do
    resources :grades
  end

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
